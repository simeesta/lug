package lug;

public class FloydWarshallMatrix extends AdjacencyMatrix {

	FloydWarshallMatrix(double[][] aMatrix) {
		super(aMatrix);
		this.aMatrix = floydWarshall(aMatrix);
		// TODO Auto-generated constructor stub
	}
	
	FloydWarshallMatrix(AdjacencyMatrix a)
	{
		super(a.aMatrix);
		this.aMatrix = this.floydWarshall(this.aMatrix);
	}
	
	private double[][] floydWarshall(double[][] aMatrix) {
		double[][] fMatrix = new double[aMatrix.length][aMatrix.length];
		
		for(int i = 0; i < aMatrix.length; i++)
			for(int j = 0; j < aMatrix.length; j++)
				fMatrix[i][j] = aMatrix[i][j];
		
		for (int k = 0; k < aMatrix.length; k++)
			for (int i = 0; i < aMatrix.length; i++)
				for (int j = 0; j < aMatrix.length; j++) {
					fMatrix[i][j] = min(fMatrix[i][j], fMatrix[i][k],
							fMatrix[k][j]);
				}
		return fMatrix;

	}
	
	void addEdge(double length, int posx, int posy)
	{
		aMatrix[posx][posy] = length;
		aMatrix = floydWarshall(aMatrix);
	}



	private double min(double curr, double sum1, double sum2) {
		if (sum1 <= 0 || sum2 <= 0) {
			return curr;
		} 
		else if (curr <= 0) {
			return sum1 + sum2;
		} 
		else
			return Math.min(curr, sum1 + sum2);
	}

}
