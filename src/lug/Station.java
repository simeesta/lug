package lug;

import java.util.Vector;

public class Station {
	String stationName;
	int stationId, degree;
	// /
	double closenessCentDist, closenessCentUT, closenessCentIT,
			closenessCentPT, closenessCent, betweenCentDist, betweenCentT,
			betweenCentIT, betweenCentPT, betweenCent;

	Vector<Edge> edges;

	Station(String name, int id) {
		stationName = name;
		stationId = id;
		edges = new Vector<Edge>();
		degree = 0;
		// /
		closenessCentDist = 0;
		closenessCentUT = 0;
		closenessCentIT = 0;
		closenessCentPT = 0;
		closenessCent = 0;
		betweenCentDist = 0;
		betweenCentT = 0;
		betweenCentIT = 0;
		betweenCentPT = 0;
		betweenCent = 0;
	}

	void addEdge(Edge e) {
		edges.add(e);
		degree++;
	}

}
