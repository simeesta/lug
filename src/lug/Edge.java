package lug;

public class Edge {
	Station from;
	Station to;
	double distance, time, avgPeakTime, interPeakTime;

	/* Distance in km, times in minutes */
	Edge(Station from, Station to, double distance, double time,
			double avgPeakTime, double interPeakTime) {
		this.from = from;
		this.to = to;
		this.distance = distance;
		this.time = time;
		this.avgPeakTime = avgPeakTime;
		this.interPeakTime = interPeakTime;
	}
}
