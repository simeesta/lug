package lug;


public class AdjacencyMatrix {
	protected double[][] aMatrix;
	int size;
	AdjacencyMatrix(double[][] aMatrix)
	{
		this.aMatrix = new double[aMatrix.length][aMatrix.length];
		size = aMatrix.length;
		for(int i=0; i<size;i++)
			for(int j=0;j < size;j++)
				this.aMatrix[i][j] = aMatrix[i][j]; 
	}
	
	double get(int i, int j)
	{
		return aMatrix[i][j];
	}

	

	
}
