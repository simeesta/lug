package lug;

import java.util.*;
import java.io.*;

public class Lug {

	/**
	 * @param args
	 */
	HashMap<String, Integer> stationIds;
	Vector<Station> stations;
	// AdjacencyMatrix aMatrix;
	AdjacencyMatrix dist, utime, iPTime, pTime, simple;
	FloydWarshallMatrix fDist, fUtime, fIPTime, fPTime, fsimple;
	// AdjacencyMatrix floydWarshallMatrix;
	/*
	 * Variables for characteristic path length for distance, uninterupted time,
	 * avg Peak time and avg inter-peak time
	 */
	double avgPLengthDist, avgPLengthUTime, avgPLengthPTime, avgPLengthITime,
			avgPLength;
	// /
	double globalEfficiency;
	double globalEffDist, globalEffUTime, globalEffPTime, globalEffITime;
	double sumDist;
	double sumUT;
	double sumPT;
	double sumIT, sumSimple;

	/* Calculates characteristic path length from adjacency matrix */
	private double calcPLength(AdjacencyMatrix aMatrix) {
		double pLength = 0;
		for (int i = 0; i < aMatrix.size; i++) {
			for (int j = 0; j < aMatrix.size; j++) {
				if (i == j)
					continue;
				pLength += aMatrix.get(i, j);

			}
		}
		int n = aMatrix.size * (aMatrix.size - 1);
		pLength /= n;
		return pLength;
	}

	/* Calculates global efficiency from floyd-warshall matrix */
	private double calcGlobEff(FloydWarshallMatrix fMatrix) {
		double globEff = 0;
		for (int i = 0; i < fMatrix.size; i++) {
			for (int j = 0; j < fMatrix.size; j++) {
				if (i == j)
					continue;
				globEff += 1 / (fMatrix.get(i, j));

			}
		}
		int n = fMatrix.size * (fMatrix.size - 1);
		globEff /= n;
		return globEff;
	}

	/*
	 * Calculates the closeness centrality of a station using the floyd warshall
	 * matrix
	 */
	private double calcClosenessCent(Station s, double[][] fMatrix) {
		double closeCent = 0;
		for (int j = 0; j < fMatrix.length; j++) {
			if (s.stationId == j)
				continue;
			closeCent += fMatrix[s.stationId][j];

		}
		int n = fMatrix.length - 1;
		return n / closeCent;

	}

	/*
	 * Aproximate ratio of sigma(s,t)(v)/sigma(s,t) == 1 as unlikey to have
	 * multiple paths with same distance
	 */
	private double[] calcBetweenessCentsAprox(FloydWarshallMatrix fMatrix) {
		int size = stations.size();
		double[] betweeness = new double[size];

		for (int i = 0; i < size; i++)
			betweeness[i] = 0;

		for (int i = 0; i < stations.size(); i++) {
			for (int j = 0; j < stations.size(); j++) {
				if (i == j)
					continue;
				for (int k = 0; k < stations.size(); k++) {
					if (i == k || j == k)
						continue;
					if (fMatrix.get(i, j) == fMatrix.get(i, k)
							+ fMatrix.get(k, j)) {
						betweeness[k]++;
					}
				}
			}
		}
		return betweeness;
	}

	/*
	 * Algorithm found at
	 * http://www.inf.uni-konstanz.de/algo/publications/b-fabc-01.pdf
	 */
	public void calcBetweenessCentality() {
		double[] betweenessCentrality = new double[stations.size()];
		for (int i = 0; i < stations.size(); i++) {
			betweenessCentrality[i] = 0;
		}
		for (Station s : stations) {
			Stack<Station> stack = new Stack<Station>();
			Vector<Station>[] list = new Vector[273];
			for (int i = 0; i < stations.size(); i++) {
				list[i] = new Vector<Station>();
			}
			int[] sigma = new int[stations.size()];
			int[] delta = new int[stations.size()];
			for (Station t : stations) {

				sigma[t.stationId] = 0;
				delta[t.stationId] = -1;
			}
			sigma[s.stationId] = 1;
			delta[s.stationId] = 0;

			Queue<Station> queue = new LinkedList<Station>();
			queue.add(s);
			while (!queue.isEmpty()) {
				Station v = queue.remove();
				stack.push(v);

				for (Edge e : v.edges) {
					// e.to found for the first time?
					if (delta[e.to.stationId] < 0) {
						queue.add(e.to);
						delta[e.to.stationId] = delta[v.stationId] + 1;
					}
					// shortest path to e.to via v?
					if (delta[e.to.stationId] == delta[v.stationId] + 1) {
						sigma[e.to.stationId] += sigma[v.stationId];
						list[e.to.stationId].add(v);
					}

				}

			}

			for (Station v : stations) {
				delta[v.stationId] = 0;
			}
			// S returns vertices in oder of non-increasing dist from s
			while (!stack.isEmpty()) {
				Station w = stack.pop();
				for (Station v : list[w.stationId]) {
					delta[v.stationId] += sigma[v.stationId]
							/ sigma[w.stationId] * (1 + delta[w.stationId]);
					if (s.stationId != w.stationId) {
						betweenessCentrality[w.stationId] += delta[w.stationId];
					}
				}
			}
		}
		// Maybe return array for more reuseability ??
		for (int i = 0; i < stations.size(); i++) {
			stations.get(i).betweenCent = betweenessCentrality[i];
		}
	}

	enum Metric {
		DIST, TIME, ITIME, PTIME, SIMPLE
	}

	double[][] getAMatrix(Metric type) {
		int size = stations.size();
		double matrix[][] = new double[size][size];
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				matrix[i][j] = 0;
		for (Station s : stations) {
			int from = s.stationId;
			for (Edge e : s.edges) {
				int to = e.to.stationId;
				switch (type) {
				case DIST:
					matrix[from][to] = e.distance;
					break;
				case ITIME:
					matrix[from][to] = e.interPeakTime;
					break;
				case PTIME:
					matrix[from][to] = e.avgPeakTime;
					break;
				case TIME:
					matrix[from][to] = e.time;
					break;
				default:
					matrix[from][to]++;
				}
			}
		}

		return matrix;
	}

	Lug() throws IOException {
		stationIds = new HashMap<String, Integer>();
		stations = new Vector<Station>();
		this.readCSV();

		dist = new AdjacencyMatrix(getAMatrix(Metric.DIST));
		utime = new AdjacencyMatrix(getAMatrix(Metric.TIME));
		iPTime = new AdjacencyMatrix(getAMatrix(Metric.ITIME));
		pTime = new AdjacencyMatrix(getAMatrix(Metric.PTIME));
		simple = new AdjacencyMatrix(getAMatrix(Metric.SIMPLE));

		fDist = new FloydWarshallMatrix(dist);
		fUtime = new FloydWarshallMatrix(utime);
		fIPTime = new FloydWarshallMatrix(iPTime);
		fPTime = new FloydWarshallMatrix(pTime);
		fsimple = new FloydWarshallMatrix(simple);

		// Calculate characteristic path lengths
		avgPLengthDist = calcPLength(fDist);
		avgPLengthUTime = calcPLength(fUtime);
		avgPLengthPTime = calcPLength(fPTime);
		avgPLengthITime = calcPLength(fIPTime);
		avgPLength = calcPLength(fsimple);
		// Calculate global efficiencies
		globalEffDist = calcGlobEff(fDist);
		globalEffUTime = calcGlobEff(fUtime);
		globalEffPTime = calcGlobEff(fPTime);
		globalEffITime = calcGlobEff(fIPTime);
		globalEfficiency = calcGlobEff(fsimple);

	}
	
	public void calcCentralities()
	{
		// Calculate closeness centralities
		for (Station s : stations) {
			s.closenessCentDist = calcClosenessCent(s, fDist.aMatrix);
			s.closenessCentUT = calcClosenessCent(s, fUtime.aMatrix);
			s.closenessCentPT = calcClosenessCent(s, fPTime.aMatrix);
			s.closenessCentIT = calcClosenessCent(s, fIPTime.aMatrix);
			s.closenessCent = calcClosenessCent(s, fsimple.aMatrix);
		}
		double[] betweeness = calcBetweenessCentsAprox(fsimple);
		double[] betweenessP = calcBetweenessCentsAprox(fPTime);
		double[] betweenessI = calcBetweenessCentsAprox(fIPTime);
		double[] betweenessU = calcBetweenessCentsAprox(fUtime);
		double[] betweenessD = calcBetweenessCentsAprox(fDist);

		for (Station s : stations) {
			s.betweenCent = betweeness[s.stationId];
			s.betweenCentDist = betweenessD[s.stationId];
			s.betweenCentIT = betweenessI[s.stationId];
			s.betweenCentT = betweenessU[s.stationId];
			s.betweenCentPT = betweenessP[s.stationId];
		}
		calcBetweenessCentality();

		sumDist = getPrim(dist.aMatrix);
		sumUT = getPrim(utime.aMatrix);
		sumPT = getPrim(pTime.aMatrix);
		sumIT = getPrim(iPTime.aMatrix);
		sumSimple = getPrim(simple.aMatrix);

	}

	public double getPrim(double[][] aMatrix) {
		int size = aMatrix.length;
		double[][] matrix = new double[size][size];
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				matrix[i][j] = aMatrix[i][j];
		Vector<Integer> index = new Vector<Integer>();
		index.add(0);
		Boolean[] deleted = new Boolean[matrix.length];
		double sum = 0;

		for (int i = 0; i < matrix.length; i++) {
			deleted[i] = false;
		}

		for (int i = 0; i < matrix.length; i++) {
			// delete the row corresponding to the selected vertex
			for (int j = 0; j < matrix.length; j++) {
				matrix[index.lastElement()][j] = -1;
			}
			deleted[index.lastElement()] = true;
			// get the smallest value in the selected column and its index(row
			// number)
			double value = 999;
			int tmpIndex = -1;
			for (Integer ind : index)
				for (int j = 0; j < matrix.length; j++) {
					if (matrix[j][ind] > 0 && matrix[j][ind] < value) {
						value = matrix[j][ind];
						tmpIndex = j;
					}
				}
			index.add(tmpIndex);
			if (index.lastElement() == -1) {
				break;
			}
			if (value != 999) {
				sum += value;
			}
		}
		return sum;

	}

	public static boolean equals(double a, double b) {
		return a == b ? true : Math.abs(a - b) < 0.01;
	}

	private void writeAMatrices() throws IOException {

		FileWriter oStream = new FileWriter("aMatrices");
		BufferedWriter bw = new BufferedWriter(oStream);
		String s = "AMatrixTime = [";
		bw.write(s);
		int size = this.stations.size();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(utime.get(i,j)!=0)
				bw.write(utime.get(i,j) + " ");
				else
					bw.write("0 ");
			}
			bw.write(";\n");
		}
		bw.write("]\n\n");
		bw.write("AMatrixDist = [ ");
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(dist.get(i,j)!=0)
					bw.write(dist.get(i,j) + " ");
					else
						bw.write("0 ");
			}
			bw.write(";\n");
		}
		bw.write("]\n\n");
		bw.write("AMatrixPeakTime = [ ");
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(pTime.get(i,j)!=0)
					bw.write(pTime.get(i,j) + " ");
					else
						bw.write("0 ");
			}
			bw.write(";\n");
		}
		bw.write("]\n\n");
		bw.write("AMatrixinterPeakTime = [ ");
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(iPTime.get(i,j)!=0)
					bw.write(iPTime.get(i,j) + " ");
					else
						bw.write("0 ");
			}
			bw.write(";\n");
		}
		bw.write("]\n\n");

		bw.write("AMatrix = [ ");
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
					bw.write(simple.get(i,j) + " ");
			}
			bw.write(";\n");
		}
		bw.write("]\n\n");
		bw.close();

	}

	/* reads in the csv file and creates the stations */
	private void readCSV() throws IOException {
		FileInputStream inputStream = new FileInputStream(
				"src/lug/stationdbCSV.csv");
		BufferedReader br = new BufferedReader(new InputStreamReader(
				inputStream));
		// remove first line
		br.readLine();

		String s;
		int id = 0;
		/* read in CSV file */
		while ((s = br.readLine()) != null) {
			// First element of edgeDate is the empty string ""
			String[] edgeData = s.split("[\",]+");
			String stationNameFrom = edgeData[3];
			String stationNameTo = edgeData[4];
			// Create stations if they don't already exist
			if (!stationIds.containsKey(stationNameFrom)) {
				stationIds.put(stationNameFrom, id);
				stations.add(new Station(stationNameFrom, id++));
			}
			if (!stationIds.containsKey(stationNameTo)) {
				stationIds.put(stationNameTo, id);
				stations.add(new Station(stationNameTo, id++));
			}
			// Create new edge from edge data and add it to the station
			Station from = stations.get(stationIds.get(stationNameFrom));
			Station to = stations.get(stationIds.get(stationNameTo));
			Double distance = Double.parseDouble(edgeData[5]);
			Double time = Double.parseDouble(edgeData[6]);
			Double avgPeakTime = Double.parseDouble(edgeData[7]);
			Double interPeakTime = Double.parseDouble(edgeData[8]);
			from.addEdge(new Edge(from, to, distance, time, avgPeakTime,
					interPeakTime));

		}
	}

	public void writeResults(Lug underground) throws IOException {
		FileWriter output = new FileWriter("results.csv");
		BufferedWriter out = new BufferedWriter(output);
		out.write( "\"Station\",,\"Degree\",,\"Closeness Centrality Distance\"," +
				"\"Closeness Centrality Time\",\"Closeness Centrality Peak " +
				"Time\",\"Closeness Centrality Off-Peak Time\",\"Closeness " +
				"Centrality Simple\",,\"Betweenness Centrality Distance\",\"" +
				"Betweenness Centrality Time\",\"Betweenness Centrality Peak " +
				"Time\",\"Betweenness Peak Time\",\"Betweenness Centrality Simple\",\n");
		
		for (int i = 0; i < stations.size(); i++) {
			out.write( "\"" + stations.get(i).stationName + "\"," + ","
					+ stations.get(i).degree + "," + ","
					+ stations.get(i).closenessCentDist + ","
					+ stations.get(i).closenessCentUT + ","
					+ stations.get(i).closenessCentPT + ","
					+ stations.get(i).closenessCentIT + ","
					+ stations.get(i).closenessCent + "," + ","
					+ stations.get(i).betweenCentDist + ","
					+ stations.get(i).betweenCentT + ","
					+ stations.get(i).betweenCentPT + ","
					+ stations.get(i).betweenCentIT + ","
					+ stations.get(i).betweenCent + "\n");
		}

		out.write( "\n\n\n\n" + "\"Char Path Length Distance\"" + ","
				+ avgPLengthDist + "\n" + "\"Char Path Length Time\"" + ","
				+ avgPLengthUTime + "\n" + "\"Char Path Length PeakTime\""
				+ "," + avgPLengthPTime + "\n"
				+ "\"Char Path Length OffPeakTime\"" + "," + avgPLengthITime
				+ "\n" + "\"Char Path Length Simple\"" + "," + avgPLength
				+ "\n");

		out.write( "\n\n\n\n" + "\"Global Efficiency Distance\"" + ","
				+ globalEffDist + "\n" + "\"Global Efficiency Time\"" + ","
				+ globalEffUTime + "\n" + "\"Global Efficiency PeakTime\""
				+ "," + globalEffPTime + "\n"
				+ "\"Global Efficiency OffPeakTime\"" + "," + globalEffITime
				+ "\n" + "\"Global Efficiency\"" + "," + globalEfficiency
				+ "\n");
		out.close();

		System.out.println("Characteristic path lengh for distance is "
				+ avgPLengthDist);
		System.out
				.println("Characteristic path lengh for uninterupted time is "
						+ avgPLengthUTime);
		System.out.println("Characteristic path lengh for avgPeakTime is "
				+ avgPLengthPTime);
		System.out.println("Characteristic path lengh for avgOffPeakTime is "
				+ avgPLengthITime);

		System.out.println("");
		System.out
				.println("Global efficiency for distance is " + globalEffDist);
		System.out.println("Global efficiency for uninterupted time  is "
				+ globalEffUTime);
		System.out.println("Global efficiency for avgPeakTime is "
				+ globalEffPTime);
		System.out.println("Global efficiency for avgOffPeakTime is "
				+ globalEffITime);

		System.out.println("The Prim value for distance is " + sumDist);
		System.out.println("The Prim value for iTime is " + sumIT);
		System.out.println("The Prim value for pTime is " + sumPT);
		System.out.println("The Prim value for uTime is " + sumUT);
		System.out.println("The Prim value for simple is " + sumSimple);
		writeAMatrices();
	}

	public static void main(String[] args) {
		try {
			Lug underground = new Lug();
			underground.calcCentralities();
			underground.writeResults(underground);

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("ERROR : " + e.getMessage());
		}
	}
}
